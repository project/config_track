<?php

namespace Drupal\config_track\Controller;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\MemoryStorage;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Diff\DiffFormatter;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a generic implementation to build a listing of entities.
 */
class ConfigTrackController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * The config manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The number of entries to list per page, or FALSE to list all.
   *
   * For example, set this to FALSE if the list uses client-side filters that
   * require all entries to be listed.
   *
   * @var int|false
   */
  protected $limit = 50;

  /**
   * Constructs a ConfigTrackController object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager.
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The config manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(Connection $database, PagerManagerInterface $pager_manager, ConfigManagerInterface $config_manager, ConfigFactoryInterface $config_factory) {
    $this->database = $database;
    $this->pagerManager = $pager_manager;
    $this->configManager = $config_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('pager.manager'),
      $container->get('config.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * Builds the header row for the entity listing.
   *
   * @return array
   *   A render array structure of header strings.
   *
   * @see \Drupal\Core\Entity\EntityListBuilder::render()
   */
  public function buildHeader() {
    $row['timestamp'] = $this->t('Date');
    $row['revision_id'] = $this->t('Revision');
    $row['name'] = $this->t('Name');
    $row['collection'] = $this->t('Collection');
    $row['operation'] = $this->t('Operation');
    $row['uid'] = $this->t('User');
    $row['operations'] = $this->t('Operations');
    return $row;
  }

  /**
   * Renders the overivew.
   */
  public function overview() {
    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->t('Config revisions'),
      '#rows' => [],
      '#empty' => $this->t('There are no config revisions yet.'),
    ];

    $page = $this->pagerManager->findPage();
    $total_entries = $config_tracks = $this->database->select('config_track', 'cv')
      ->countQuery()
      ->execute()
      ->fetchField();
    $this->pagerManager->createPager($total_entries, $this->limit);

    $config_tracks = $this->database->select('config_track', 'cv')
      ->fields('cv', [
        'timestamp',
        'revision_id',
        'name',
        'collection',
        'operation',
        'uid',
      ])
      ->orderBy('timestamp', 'DESC')
      ->orderBy('revision_id', 'DESC')
      ->range($page * $this->limit, $this->limit)
      ->execute()
      ->fetchAllAssoc('revision_id', \PDO::FETCH_ASSOC);
    $build['table']['#rows'] = $config_tracks;

    foreach ($build['table']['#rows'] as $revision_id => &$row) {
      $row['timestamp'] = DateTimePlus::createFromTimestamp($row['timestamp'])->format(DateTimePlus::FORMAT);
      $row['uid'] = User::load($row['uid'])->toLink();
      $row['operations']['data'] = [
        '#type' => 'operations',
        '#links' => [
          [
            'title' => $this->t('Details'),
            'url' => Url::fromRoute('config_track.config_track.single', ['revision_id' => $revision_id]),
          ]
        ],
      ];
    }

    $build['pager_pager'] = ['#type' => 'pager'];
    $build['#cache']['tags'] = ['config-revision-list'];
    return $build;
  }

  /**
   * The revision diff to a previous revision.
   *
   * @param $revision_id
   *   The revision diff.
   *
   * @return array
   *   The render array.
   */
  public function revision($revision_id) {
    $config_track = $this->database->select('config_track', 'cv')
      ->fields('cv', [
        'timestamp',
        'revision_id',
        'name',
        'collection',
        'operation',
        'uid',
        'data',
      ])
      ->condition('revision_id', $revision_id)
      ->execute()
      ->fetchAssoc(\PDO::FETCH_ASSOC);

    $previous_config_track = $this->database->select('config_track', 'cv')
      ->fields('cv', [
        'timestamp',
        'revision_id',
        'name',
        'collection',
        'operation',
        'uid',
        'data',
      ])
      ->condition('name', $config_track['name'])
      ->condition('collection', $config_track['collection'])
      ->condition('revision_id', $revision_id, '<')
      ->orderBy('timestamp', 'DESC')
      ->orderBy('revision_id', 'DESC')
      ->range(0, 1)
      ->execute()
      ->fetchAssoc(\PDO::FETCH_ASSOC);

    $config_track_data = unserialize($config_track['data'], ['allowed_classes' => FALSE]);
    $previous_config_track_data = $previous_config_track ? unserialize($previous_config_track['data'], ['allowed_classes' => FALSE]) : [];
    $diff = $this->getDiff($previous_config_track_data, $config_track_data, $config_track['name']);
    // Drupal\Component\Diff\DiffFormatter does not expose a service so we
    // instantiate it manually here.
    $diff_formatter = new DiffFormatter($this->configFactory);
    $diff_formatter->show_header = FALSE;

    $build['#title'] = t('Config revision diff @config_file', ['@config_file' => $config_track['name']]);
    // Add the CSS for the inline diff.
    $build['#attached']['library'][] = 'system/diff';
    $build['diff'] = [
      '#type' => 'table',
      '#attributes' => [
        'class' => ['diff'],
      ],
      '#header' => [
        ['data' => t('Previous'), 'colspan' => '2'],
        ['data' => t('Current'), 'colspan' => '2'],
      ],
      '#rows' => $diff_formatter->format($diff),
    ];

    return $build;
  }

  /**
   * Gets a diff of two config versions.
   *
   * @param array $previous_config_track_data
   *   The previous config data.
   * @param array $config_track_data
   *   The current config data.
   * @param $name
   *   The config name.
   *
   * @return \Drupal\Component\Diff\Diff
   *   The diff of both configs.
   */
  public function getDiff(array $previous_config_track_data, array $config_track_data, $name) {
    // Since the config manager diff method works only with storages we need to
    // simulate the functionality by using a memory storage.
    $source_storage = new MemoryStorage();
    if (!empty($previous_config_track_data)) {
      $source_storage->write($name, $previous_config_track_data);
    }
    $target_storage = new MemoryStorage();
    if (!empty($config_track_data)) {
      $target_storage->write($name, $config_track_data);
    }
    return $this->configManager->diff($source_storage, $target_storage, $name);
  }

  /**
   * Title callback for the revision page.
   *
   * @param $revision_id
   *   The revision ID.
   *
   * @return string
   *   The name of the config.
   */
  public function revisionTitle($revision_id) {
    $name = $this->database->select('config_track', 'cv')
      ->fields('cv', [
        'name',
      ])
      ->condition('revision_id', $revision_id)
      ->execute()
      ->fetchField();
    return $this->t('Config Revision %revision_id: %name', ['%revision_id' => $revision_id, '%name' => $name]);
  }

}
