<?php

/**
 * @file
 * Contains Drupal\config_track\EventSubscriber\ConfigLogDatabaseSubscriber.
 */

namespace Drupal\config_track\EventSubscriber;

use Drupal\config_track\Config\ConfigTrackLanguageConfigOverride;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\language\Config\LanguageConfigOverrideCrudEvent;
use Drupal\language\Config\LanguageConfigOverrideEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * ConfigTrack subscriber for configuration CRUD events.
 */
class ConfigTrackSubscriber implements EventSubscriberInterface {

  /**
   * The config manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * Constructs the ConfigTrackSubscriber object.
   *
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The config manager services.
   */
  public function __construct(ConfigManagerInterface $config_manager) {
    $this->configManager = $config_manager;
  }

  /**
   * React to configuration ConfigEvent::SAVE events.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The event to process.
   *
   * @throws \Exception
   */
  public function onConfigSave(ConfigCrudEvent $event) {
    $config = $event->getConfig();
    $name = $config->getName();
    // We take care only of simply configs, the others are being covered by
    // entity API hooks in order to maintian the save order properly.
    if ($this->configManager->getEntityTypeIdByName($name) !== NULL) {
      return;
    }

    $config_track =& drupal_static('config_track', []);

    // Only register the shutdown function once.
    if (empty($config_track)) {
      drupal_register_shutdown_function('config_track_shutdown');
    }

    $config_track[] = [
      'name' => $name,
      'operation' => $config->isNew() ? 'create' : 'update',
      'collection' => $config->getStorage()->getCollectionName(),
      'data' => $config->get(),
      'original_data' => $config->getOriginal(),
    ];
  }

  /**
   * React to configuration ConfigEvent::DELETE events.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The event to process.
   *
   * @throws \Exception
   */
  public function onConfigDelete(ConfigCrudEvent $event) {
    $config = $event->getConfig();
    $name = $config->getName();
    // We take care only of simply configs, the others are being covered by
    // entity API hooks in order to maintian the save order properly.
    if ($this->configManager->getEntityTypeIdByName($name) !== NULL) {
      return;
    }

    $config_track =& drupal_static('config_track', []);

    // Only register the shutdown function once.
    if (empty($config_track)) {
      drupal_register_shutdown_function('config_track_shutdown');
    }

    $config_track[] = [
      'name' => $name,
      'operation' => 'delete',
      'collection' => $config->getStorage()->getCollectionName(),
      'data' => NULL,
    ];
  }

  /**
   * React to configuration LanguageConfigOverrideEvents::SAVE_OVERRIDE events.
   *
   * @param \Drupal\language\Config\LanguageConfigOverrideCrudEvent $event
   *   The event to process.
   *
   * @throws \Exception
   */
  public function onLanguageConfigOverrideSave(LanguageConfigOverrideCrudEvent $event) {
    $config = $event->getLanguageConfigOverride();
    $name = $config->getName();
    $config_track =& drupal_static('config_track', []);

    // Only register the shutdown function once.
    if (empty($config_track)) {
      drupal_register_shutdown_function('config_track_shutdown');
    }

    $config_track[] = [
      'name' => $name,
      'operation' => $config->isNew() ? 'create' : 'update',
      'collection' => $config->getStorage()->getCollectionName(),
      'data' => $config->get(),
      'original_data' => ConfigTrackLanguageConfigOverride::getOriginalByConfig($config),
    ];
  }

  /**
   * React to configuration LanguageConfigOverrideEvents::DELETE_OVERRIDE events.
   *
   * @param \Drupal\language\Config\LanguageConfigOverrideCrudEvent $event
   *   The event to process.
   *
   * @throws \Exception
   */
  public function onLanguageConfigOverrideDelete(LanguageConfigOverrideCrudEvent $event) {
    $config = $event->getLanguageConfigOverride();
    $name = $config->getName();

    $config_track =& drupal_static('config_track', []);

    // Only register the shutdown function once.
    if (empty($config_track)) {
      drupal_register_shutdown_function('config_track_shutdown');
    }

    $config_track[] = [
      'name' => $name,
      'operation' => 'delete',
      'collection' => $config->getStorage()->getCollectionName(),
      'data' => NULL,
    ];
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents(): array {
    $events[ConfigEvents::SAVE][] = ['onConfigSave', 10];
    $events[ConfigEvents::DELETE][] = ['onConfigDelete', 10];

    if (class_exists('\Drupal\language\Config\LanguageConfigOverrideEvents')) {
      $events[LanguageConfigOverrideEvents::SAVE_OVERRIDE][] = ['onLanguageConfigOverrideSave', 10];
      $events[LanguageConfigOverrideEvents::DELETE_OVERRIDE][] = ['onLanguageConfigOverrideDelete', 10];
    }
    return $events;
  }

}
