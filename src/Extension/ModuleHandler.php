<?php

namespace Drupal\config_track\Extension;

use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Decorates the core module handler.
 *
 * We need our entity hooks to run once before and once after all other module
 * hook implementations.
 */
class ModuleHandler implements ModuleHandlerInterface {

  /**
   * The decorated http_client service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $innerModuleHandler;

  /**
   * The entity last installed schema repository.
   *
   * @var \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface
   */
  protected $entityLastInstalledSchemaRepository;

  /**
   * An list of all config entity type IDs.
   *
   * @var array
   */
  protected $configEntityTypes = [];

  /**
   * Constructs a ModuleHandler object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface
   *   The decorated module handler service.
   * @param \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface $entity_last_installed_schema_repository
   *   The entity last installed schema repository.
   */
  public function __construct(ModuleHandlerInterface $inner_module_handler, EntityLastInstalledSchemaRepositoryInterface $entity_last_installed_schema_repository) {
    $this->innerModuleHandler = $inner_module_handler;
    // We use the entity last installed schema repository service instead of the
    // entity type manager as otherwise there is a circular dependency created.
    $this->entityLastInstalledSchemaRepository = $entity_last_installed_schema_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function invokeAll($hook, array $args = []) {
    // The specific entity type hooks are called first.
    $parts = explode('_presave', $hook);
    if(count($parts) === 2 && !empty($this->getConfigEntityTypeIds()[$parts[0]])) {
      config_track_entity_presave_first(...$args);
    }
    else {
      $parts = explode('_predelete', $hook);
      if(count($parts) === 2 && !empty($this->getConfigEntityTypeIds()[$parts[0]])) {
        config_track_entity_predelete_first(...$args);
      }
    }

    $return = $this->innerModuleHandler->invokeAll($hook, $args);

    // The general entity hooks are called last.
    if($hook === 'entity_presave') {
      config_track_entity_presave_last(...$args);
    }
    else if($hook === 'entity_predelete') {
      config_track_entity_predelete_last(...$args);
    }

    return $return;
  }

  /**
   * Returns a list of all config entity type IDs.
   *
   * @return array
   *   All config entity type IDs in the system.
   */
  protected function getConfigEntityTypeIds() {
    if(empty($this->configEntityTypes)) {
      foreach($this->entityLastInstalledSchemaRepository->getLastInstalledDefinitions() as $entity_type_id => $entity_type) {
        if($entity_type instanceof ConfigEntityTypeInterface) {
          $this->configEntityTypes[$entity_type_id] = $entity_type_id;
        }
      }
    }
    return $this->configEntityTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function load($name) {
    return $this->innerModuleHandler->load($name);
  }

  /**
   * {@inheritdoc}
   */
  public function loadAll() {
    $this->innerModuleHandler->loadAll();
  }

  /**
   * {@inheritdoc}
   */
  public function isLoaded() {
    return $this->innerModuleHandler->isLoaded();
  }

  /**
   * {@inheritdoc}
   */
  public function reload() {
    $this->innerModuleHandler->reload();
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleList() {
    return $this->innerModuleHandler->getModuleList();
  }

  /**
   * {@inheritdoc}
   */
  public function getModule($name) {
    return $this->innerModuleHandler->getModule($name);
  }

  /**
   * {@inheritdoc}
   */
  public function setModuleList(array $module_list = []) {
    $this->innerModuleHandler->setModuleList($module_list);
  }

  /**
   * {@inheritdoc}
   */
  public function addModule($name, $path) {
    $this->innerModuleHandler->addModule($name, $path);
  }

  /**
   * {@inheritdoc}
   */
  public function addProfile($name, $path) {
    $this->innerModuleHandler->addProfile($name, $path);
  }

  /**
   * {@inheritdoc}
   */
  public function buildModuleDependencies(array $modules) {
    return $this->innerModuleHandler->buildModuleDependencies($modules);
  }

  /**
   * {@inheritdoc}
   */
  public function moduleExists($module) {
    return $this->innerModuleHandler->moduleExists($module);
  }

  /**
   * {@inheritdoc}
   */
  public function loadAllIncludes($type, $name = NULL) {
    $this->innerModuleHandler->addProfile($type, $name);
  }

  /**
   * {@inheritdoc}
   */
  public function loadInclude($module, $type, $name = NULL) {
    return $this->innerModuleHandler->loadInclude($module, $type, $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getHookInfo() {
    return $this->innerModuleHandler->getHookInfo();
  }

  /**
   * {@inheritdoc}
   */
  public function getImplementations($hook) {
    return $this->innerModuleHandler->getImplementations($hook);
  }

  /**
   * {@inheritdoc}
   */
  public function writeCache() {
    $this->innerModuleHandler->writeCache();
  }

  /**
   * {@inheritdoc}
   */
  public function resetImplementations() {
    $this->innerModuleHandler->resetImplementations();
  }

  /**
   * {@inheritdoc}
   */
  public function hasImplementations(string $hook, $modules = NULL): bool {
    return $this->innerModuleHandler->hasImplementations($hook, $modules);
  }

  /**
   * {@inheritdoc}
   */
  public function implementsHook($module, $hook) {
    return $this->innerModuleHandler->implementsHook($module, $hook);
  }

  /**
   * {@inheritdoc}
   */
  public function invokeAllWith(string $hook, callable $callback): void {
    $this->innerModuleHandler->invokeAllWith($hook, $callback);
  }

  /**
   * {@inheritdoc}
   */
  public function invoke($module, $hook, array $args = []) {
    return $this->innerModuleHandler->invoke($module, $hook, $args);
  }

  /**
   * {@inheritdoc}
   */
  public function invokeDeprecated($description, $module, $hook, array $args = []) {
    return $this->innerModuleHandler->invokeDeprecated($description, $module, $hook, $args);
  }

  /**
   * {@inheritdoc}
   */
  public function invokeAllDeprecated($description, $hook, array $args = []) {
    return $this->innerModuleHandler->invokeAllDeprecated($description, $hook, $args);
  }

  /**
   * {@inheritdoc}
   */
  public function alter($type, &$data, &$context1 = NULL, &$context2 = NULL) {
    $this->innerModuleHandler->alter($type, $data, $context1, $context2);
  }

  /**
   * {@inheritdoc}
   */
  public function alterDeprecated($description, $type, &$data, &$context1 = NULL, &$context2 = NULL) {
    $this->innerModuleHandler->alterDeprecated($description, $type, $data, $context1, $context2);
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleDirectories() {
    return $this->innerModuleHandler->getModuleDirectories();
  }

  /**
   * {@inheritdoc}
   */
  public function getName($module) {
    return $this->innerModuleHandler->getName($module);
  }

  /**
   * {@inheritdoc}
   */
  public function destruct() {
    $this->innerModuleHandler->destruct();
  }

}
