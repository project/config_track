<?php

namespace Drupal\config_track\Config;

use Drupal\language\Config\LanguageConfigOverride;

class ConfigTrackLanguageConfigOverride extends LanguageConfigOverride {

  /**
   * Gets original data from given configuration object.
   *
   * @return array
   *   The original data.
   */
  public static function getOriginalByConfig(LanguageConfigOverride $config): array {
    return $config->originalData;
  }

}
