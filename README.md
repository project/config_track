# Config Track

This module allows to log any configuration change in Drupal 9/10.

All changes to configuration in any config collection will be tracked
as well as that the order of changes will be preserved.

The module provides a nice UI where you can see a list of all recent config
changes in the system as well as the user that triggered those changes.
A diff is also available in order to see the exact change.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/config_track).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/config_track).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no modifiable settings. There is no configuration.


## Maintainers

- Hristo Chonov - [hchonov](https://www.drupal.org/u/hchonov)
